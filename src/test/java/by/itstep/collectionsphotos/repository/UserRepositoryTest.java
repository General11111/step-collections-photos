package by.itstep.collectionsphotos.repository;

import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.utils.DatabaseCleaner;
import by.itstep.collectionsphotos.utils.EntityUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private DatabaseCleaner dbCleaner;

    @BeforeEach
    public void setUp() {

        dbCleaner.clean();
    }

    @AfterEach
    public void shutDown() {
        dbCleaner.clean();
    }

    @Test
    public void create_happyPath() {
        UserEntity user = EntityUtils.prepareUser();

        UserEntity createdUser = userRepository.create(user);

        Assertions.assertNotNull(createdUser.getId());
        UserEntity foundUser = userRepository.findById(createdUser.getId());
        Assertions.assertEquals(user.getName(), foundUser.getName());
        Assertions.assertEquals(user.getPassword(), foundUser.getPassword());
        Assertions.assertEquals(user.getEmail(), foundUser.getEmail());
        Assertions.assertEquals(user.getLogin(), foundUser.getLogin());
    }

    @Test
    public void findAll_whenNoOneFound() {
        List<UserEntity> foundUsers = userRepository.findAll();

        Assertions.assertEquals(0, foundUsers.size());
        Assertions.assertTrue(foundUsers.isEmpty());
    }

    @Test
    public void findAll_happyPath() {

        addUserToDb();
        addUserToDb();

        List<UserEntity> foundUsers = userRepository.findAll();

        Assertions.assertEquals(2, foundUsers.size());
    }

    @Test
    public void update_happyPath() {
        UserEntity existingUser = addUserToDb();

        existingUser.setEmail("updated");
        existingUser.setLogin("updated");
        existingUser.setPassword("updated");

        UserEntity updateUser = userRepository.update(existingUser);

        Assertions.assertEquals(existingUser.getId(), updateUser.getId());
        UserEntity foundUser = userRepository.findById(existingUser.getId());

        Assertions.assertEquals(existingUser.getName(), foundUser.getName());
        Assertions.assertEquals(existingUser.getPassword(), foundUser.getPassword());
        Assertions.assertEquals(existingUser.getEmail(), foundUser.getEmail());
        Assertions.assertEquals(existingUser.getLogin(), foundUser.getLogin());
    }

    @Test
    public void delete_happyPath() {

        UserEntity existingUser = addUserToDb();
        userRepository.deleteById(existingUser.getId());
        UserEntity foundUser = userRepository.findById(existingUser.getId());
        Assertions.assertNull(foundUser);
    }



    private UserEntity addUserToDb() {
        UserEntity userToAdd = EntityUtils.prepareUser();
        return userRepository.create(userToAdd);
    }

}
