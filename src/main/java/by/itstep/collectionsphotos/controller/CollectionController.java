package by.itstep.collectionsphotos.controller;

import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.service.CollectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class CollectionController {

    @Autowired
    private CollectionService collectionService;

    @ResponseBody
    @RequestMapping(value = "/collections", method = RequestMethod.GET)
    public List<CollectionEntity> findAllCollections(){
        List<CollectionEntity> allCollection = collectionService.findAll();
        return allCollection;
    }

    @ResponseBody
    @RequestMapping(value = "/collections/{id}", method = RequestMethod.GET)
    public CollectionEntity findById(@PathVariable int id){
        CollectionEntity collection = collectionService.findById(id);
        return collection;
    }



}
