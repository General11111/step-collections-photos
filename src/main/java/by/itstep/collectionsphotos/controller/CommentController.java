package by.itstep.collectionsphotos.controller;

import by.itstep.collectionsphotos.dto.CommentFullDto;
import by.itstep.collectionsphotos.entity.CommentEntity;
import by.itstep.collectionsphotos.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class CommentController {

    @Autowired
    private CommentService commentService;

    @ResponseBody
    @RequestMapping(value = "/comments", method = RequestMethod.GET)
    public List<CommentFullDto> findAllComments(){
        List<CommentFullDto> allComments = commentService.findAll();
        return allComments;
    }

    @ResponseBody
    @RequestMapping(value = "/comments/{id}", method = RequestMethod.GET)
    public CommentFullDto findById(@PathVariable int id){
        CommentFullDto comment = commentService.findById(id);
        return comment;
    }

//    @ResponseBody
//    @RequestMapping(value = "/users/{userId}/photos/{photoId}/comments", method = RequestMethod.POST)
//    public CommentEntity create(@PathVariable Integer userId, @PathVariable Integer photoId, @RequestBody CommentEntity createRequest){
//        CommentEntity createdComment = commentService.create(createRequest, userId, photoId);
//        return createdComment;
//    }

    











}
