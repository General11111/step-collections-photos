package by.itstep.collectionsphotos.repository;

import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.utils.EntityManagerUtils;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class UserHibernateRepository implements UserRepository{


    @Override
    public UserEntity findById(int id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();
        UserEntity foundUser = em.find(UserEntity.class, id);
        if (foundUser != null) {
            Hibernate.initialize(foundUser.getCollection());
            Hibernate.initialize(foundUser.getComments());
        }

        em.getTransaction().commit();
        em.close();
        return foundUser;
    }

    @Override
    public List<UserEntity> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        List<UserEntity> allUsers = em
                .createNativeQuery("SELECT * FROM users", UserEntity.class)
                .getResultList();

        for (UserEntity user : allUsers) {
            Hibernate.initialize(user.getCollection());
            Hibernate.initialize(user.getComments());
        }

        em.getTransaction().commit();
        em.close();
        return allUsers;
    }

    @Override
    public UserEntity create(UserEntity entity) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.persist(entity);

        em.getTransaction().commit();
        em.close();


        return entity;
    }

    @Override
    public UserEntity update(UserEntity entity) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        em.getTransaction().begin();

        UserEntity entityToUpdate = em.find(UserEntity.class, entity.getId());
        entityToUpdate.setName(entity.getName());
        entityToUpdate.setEmail(entity.getEmail());
        entityToUpdate.setLogin(entity.getLogin());
        entityToUpdate.setPassword(entity.getPassword());

        Hibernate.initialize(entityToUpdate.getCollection());

        em.getTransaction().commit();
        em.close();
        return entityToUpdate;
    }

    @Override
    public void deleteById(int id) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        UserEntity entityToRemove = em.find(UserEntity.class, id);
        em.remove(entityToRemove);

        em.getTransaction().commit();
        em.close();

    }

    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM users")
                .executeUpdate();

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public UserEntity findByLogin(String login) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        String sql = String.format("SELECT * FROM users WHERE login = '%s';", login);
        UserEntity foundUser = (UserEntity) em
                .createNativeQuery(sql, UserEntity.class)
                .getSingleResult();

        em.getTransaction().commit();
        em.close();

        return foundUser;
    }


}
