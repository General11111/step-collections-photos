package by.itstep.collectionsphotos.repository;

import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.utils.EntityManagerUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository("jdbc")
public class CollectionJDBCRepository implements CollectionRepository {
    @Override
    public CollectionEntity findById(int id) {

        return null;
    }

    @Override
    public List<CollectionEntity> findAll() {
        return null;
    }

    @Override
    public CollectionEntity create(CollectionEntity entity) {
        return null;
    }

    @Override
    public CollectionEntity update(CollectionEntity entity) {
        return null;
    }

    @Override
    public void deleteById(int id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();
        CollectionEntity entityToRemove = em.find(CollectionEntity.class, id);
        em.remove(entityToRemove);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM collections")
                .executeUpdate();

        em.getTransaction().commit();
        em.close();
    }
}
