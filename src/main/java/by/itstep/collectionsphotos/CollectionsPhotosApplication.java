package by.itstep.collectionsphotos;

import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.UserEntity;
import by.itstep.collectionsphotos.repository.CollectionHibernateRepository;
import by.itstep.collectionsphotos.repository.CollectionRepository;
import by.itstep.collectionsphotos.repository.UserHibernateRepository;
import by.itstep.collectionsphotos.repository.UserRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
public class CollectionsPhotosApplication {

	public static void main(String[] args) {
		SpringApplication.run(CollectionsPhotosApplication.class, args);


//		UserRepository userRepository = new UserHibernateRepository();
//
//
//		UserEntity user = userRepository.findById(1);
//		System.out.println("Имя: " + user.getName());
//		System.out.println("Количество коллекций: " + user.getCollection().size());
	}

}
