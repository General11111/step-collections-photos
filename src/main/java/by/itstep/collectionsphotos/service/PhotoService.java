package by.itstep.collectionsphotos.service;

import by.itstep.collectionsphotos.dto.PhotoCreateDto;
import by.itstep.collectionsphotos.dto.PhotoFullDto;
import by.itstep.collectionsphotos.dto.PhotoShortDto;
import by.itstep.collectionsphotos.dto.PhotoUpdateDto;
import by.itstep.collectionsphotos.entity.CollectionEntity;
import by.itstep.collectionsphotos.entity.CommentEntity;
import by.itstep.collectionsphotos.entity.PhotoEntity;
import by.itstep.collectionsphotos.mapper.PhotoMapper;
import by.itstep.collectionsphotos.repository.*;
//import by.itstep.collectionsphotos.utils.ServiceFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PhotoService {

    @Autowired
    private PhotoRepository photoRepository;
    @Autowired
    private CollectionRepository collectionRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private CollectionService collectionService;
    @Autowired
    private PhotoMapper photoMapper;

    public PhotoFullDto findById(int id){
        PhotoEntity photo = photoRepository.findById(id);
        if (photo == null) {
            throw new RuntimeException("PhotoService -> Photo not found by id: " + id);
        }
        System.out.println("PhotoService -> Found photo: " + photo);
        PhotoFullDto dto = photoMapper.map(photo);
        return dto;
    }

    public List<PhotoShortDto> findAll(){
        List<PhotoEntity> allPhotos = photoRepository.findAll();
        List<PhotoShortDto> allDtos = photoMapper.map(allPhotos);
        return allDtos;
    }

    public PhotoFullDto create(PhotoCreateDto createRequest) {
        PhotoEntity photo = photoMapper.map(createRequest);
        if(photo.getId() != null){
            throw new RuntimeException("PhotoService -> Can not create photo with id.");
        }
        List<PhotoEntity> existingPhotos = photoRepository.findAll();
        for (PhotoEntity existingPhoto : existingPhotos) {
            if (existingPhoto.getName().equals(photo.getName())) {
                throw new RuntimeException("Name: " + photo.getName() + "is taken");
            }
        }
        PhotoEntity createdPhoto = photoRepository.create(photo);
        PhotoFullDto createdDto = photoMapper.map(createdPhoto);
        return createdDto;
    }

    public PhotoFullDto update(PhotoUpdateDto updateRequest){
        PhotoEntity photo = photoRepository.findById(updateRequest.getId());
        if (photo == null) {
            throw new RuntimeException("Photo is not found by id");
        }
        photo.setName(updateRequest.getName());
        PhotoEntity updatedPhoto = photoRepository.update(photo);
        PhotoFullDto dto = photoMapper.map(updatedPhoto);
        return dto;
    }

//    public void deleteById(int photoId){
//        PhotoEntity photoToDelete = photoRepository.findById(photoId);
//        if(photoToDelete==null){
//            throw new RuntimeException("PhotoService -> Photo not found by id: "+ photoId);
//        }
//
//        List<CommentEntity> commentsToPhoto = photoToDelete.getComments();
//        for(CommentEntity comment:commentsToPhoto){
//            commentRepository.deleteById(comment.getId());
//        }
//
//        List<CollectionEntity> collectionsWithPhoto = photoToDelete.getCollections();
//        for(CollectionEntity collection:collectionsWithPhoto){
//            collectionService.removePhoto(collection.getId(),photoId);
//        }
//
//        photoRepository.deleteById(photoId);
//        System.out.println("PhotoService -> Deleted photo: "+ photoToDelete);
//    }


    public void deleteById(int id) {
        PhotoEntity photo = photoRepository.findById(id);
        List<CollectionEntity> photoCollections = photo.getCollections();
        for (CollectionEntity collection : photoCollections) {
            collection.getPhotos().remove(photo);
            collectionRepository.update(collection);
        }
        photoRepository.deleteById(id);
    }
}
