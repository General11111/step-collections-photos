package by.itstep.collectionsphotos.todelete;

public class PrintingHouse {

    private int counter = 0;

    public static final PrintingHouse INSTANCE = new PrintingHouse();

    private PrintingHouse() {

    }

    public synchronized String print() {
        return "Passport: { id: " + counter++ + " }";
    }

}
