package by.itstep.collectionsphotos.todelete;

public class OneWindow {

    private PrintingHouse printingHouse;

    public OneWindow() {
        this.printingHouse = PrintingHouse.INSTANCE;
    }

    public String providePassport() {
        return printingHouse.print();
    }
}
